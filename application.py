# application.py
# created by Anna Craven on 11/27/17
# based on code written by the CS50 staff for pset7

from cs50 import SQL
from flask import Flask, flash, redirect, render_template, request, session
from flask_session import Session
from tempfile import mkdtemp
from werkzeug.exceptions import default_exceptions
from werkzeug.security import check_password_hash, generate_password_hash
from datetime import datetime

from helpers import apology, login_required, captain_only

# Configure application
app = Flask(__name__)

# Ensure responses aren't cached
if app.config["DEBUG"]:
    @app.after_request
    def after_request(response):
        response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
        response.headers["Expires"] = 0
        response.headers["Pragma"] = "no-cache"
        return response

# Configure session to use filesystem (instead of signed cookies)
app.config["SESSION_FILE_DIR"] = mkdtemp()
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

# Configure CS50 Library to use SQLite database
db = SQL("sqlite:///dport.db")


@app.route("/register", methods=["GET", "POST"])
def register():
    """Register user"""

    if request.method == "POST":

        # Access user input
        name = request.form.get("name")
        net_id = request.form.get("net_id")
        password = request.form.get("password")
        password2 = request.form.get("confirmation")
        bool_captain = request.form.get("bool_captain")
        email = request.form.get("email")

        # Make sure name field isn't blank
        if not name:
            return apology("Name field cannot be blank")

        # Make sure net_id has not already been used
        id_exists = db.execute(
            "SELECT net_id FROM users WHERE net_id = :net_id", net_id=net_id)
        if id_exists:
            return apology("Id already in use")

        # Make sure other fields are not blank
        if not name or not bool_captain or not email:
            return apology("Error: No field may be left blank")

        # Make sure password fields aren't blank
        elif not password or not password2:
            return apology("Password fields cannot be blank")

        # Check if passwords are the same
        if password != password2:
            return apology("Passwords must match")
        elif password == password2:
            # Generate hash of password
            hash1 = generate_password_hash(password)

            # Add user to database
            rows = db.execute(
                "INSERT INTO users (name, net_id, hash, email, bool_captain) VALUES(:name, :net_id, :hash, :email, :bool_captain)", name=name, net_id=net_id, hash=hash1, email=email, bool_captain=bool_captain)

            # Select user id
            user_id = db.execute(
                "SELECT user_id FROM users WHERE net_id=:net_id", net_id=net_id)

            # Log user in
            session["user_id"] = user_id[0]["user_id"]
            session["bool_captain"] = int(bool_captain)

            # Redirect user to home page
            return redirect("/")

    # User reached route via GET
    else:
        return render_template("register.html")


@app.route("/login", methods=["GET", "POST"])
def login():
    """Log user in"""

    # Forget any user_id
    session.clear()

    # User reached route via POST (as by submitting a form via POST)
    if request.method == "POST":

        # Ensure name was submitted
        if not request.form.get("net_id"):
            return apology("must provide net id", 403)

        # Ensure password was submitted
        elif not request.form.get("password"):
            return apology("must provide password", 403)

        # Query users table for net_id
        rows = db.execute(
            "SELECT * FROM users WHERE net_id = :net_id", net_id=request.form.get("net_id"))

        # Ensure name exists and password is correct
        if len(rows) != 1 or not check_password_hash(rows[0]["hash"], request.form.get("password")):
            return apology("invalid name and/or password", 403)

        # Remember which user has logged in
        session["user_id"] = rows[0]["user_id"]
        session["bool_captain"] = rows[0]["bool_captain"]

        # Redirect user to home page
        return redirect("/")

    # User reached route via GET (as by clicking a link or via redirect)
    else:
        return render_template("login.html")


@app.route("/logout")
@login_required
def logout():
    """Log user out"""

    # Forget any user_id
    session.clear()

    # Redirect user to login form
    return redirect("/")


@app.route("/", methods=["GET", "POST"])
@login_required
def index():
    """ Display team pages """

    if request.method == "POST":
        # Access the sport
        sport = request.form.get("sport")

        # Access the team id for that sport
        rows = db.execute(
            "SELECT team_id FROM teams WHERE sport = :sport", sport=sport)

        # Modify session to contain team_id
        session["team_id"] = rows[0]["team_id"]

        # Redirect user to schedule for that team
        return redirect("/schedule")

    # Show the user the teams that he or she is a part of
    else:
        # if the user is a player
        if session["bool_captain"] != 1:
            # Select the teams that the player is a part of
            teams = db.execute(
                "SELECT sport FROM teams JOIN players ON players.team_id = teams.team_id WHERE player_id = :player_id ORDER BY teams.sport", player_id=session["user_id"])
            return render_template("my_teams.html", teams=teams)
        # if the user is a captain
        else:
            # Select the teams that the captain runs
            teams = db.execute(
                "SELECT sport FROM teams WHERE captain_id = :captain_id ORDER BY sport", captain_id=session["user_id"])
            return render_template("my_teams.html", teams=teams)


@app.route("/create_team", methods=["GET", "POST"])
@login_required
@captain_only
def create_team():
    """Allow captain to create team """

    if request.method == "POST":
        # Determine the id for the team
        new_team = db.execute(
            "SELECT team_id FROM teams WHERE sport = :sport", sport=request.form.get("sport"))

        # Add captain's id to team in teams
        db.execute("UPDATE teams SET captain_id = :captain_id WHERE sport = :sport",
                   captain_id=session["user_id"], sport=request.form.get("sport"))

        # Redirect captain to My Teams
        return redirect("/")

    else:
        # Select sports that do not yet have a captain assigned
        sports = db.execute(
            "SELECT sport FROM teams WHERE captain_id IS NULL")
        return render_template("create_team.html", sports=sports)


@app.route("/dissolve_team", methods=["POST"])
@login_required
@captain_only
def dissolve_team():
    """ Allow captain to dissolve a team """

    # Set captain_id in teams table to null for that team
    db.execute("UPDATE teams SET captain_id = NULL WHERE team_id = :team_id",
               team_id=session["team_id"])

    # Delete all games associated with that team
    db.execute("DELETE FROM schedule WHERE team_id = :team_id",
               team_id=session["team_id"])
    db.execute("DELETE FROM playing WHERE team_id = :team_id",
               team_id=session["team_id"])
    db.execute("DELETE FROM players WHERE team_id = :team_id",
               team_id=session["team_id"])

    # Redirect captain to My Teams page
    return redirect("/")


@app.route("/schedule", methods=["GET"])
@login_required
def schedule():
    """Access schedule of games for a certain sport"""

    # Access games for that sport
    games = db.execute(
        "SELECT * FROM schedule JOIN teams ON teams.team_id = schedule.team_id WHERE schedule.team_id = :team_id ORDER BY date", team_id=session["team_id"])

    # Access captain for that sport
    rows = db.execute(
        "SELECT name FROM users JOIN teams ON teams.captain_id = users.user_id WHERE teams.team_id = :team_id", team_id=session["team_id"])
    captain = rows[0]["name"]

    # Access name of sport
    sport = db.execute(
        "SELECT sport FROM teams WHERE team_id = :team_id", team_id=session["team_id"])

    # Create a list to keep track of if a player has joined a game (1) or not (0)
    joined = []
    # For each game for this sport, if a player has joined it, set the corresponding value in joined to 1
    for i in range(len(games)):
        rows = db.execute(
            "SELECT * FROM playing WHERE player_id = :player_id AND game_id = :game_id", player_id=session["user_id"], game_id=games[i]["game_id"])
        if rows:
            joined.append(1)
        else:
            joined.append(0)

    # Reformat dates to be month day
    for i in range(len(games)):
        date = games[i]["date"]
        date = datetime.strptime(date, "%Y-%m-%d")
        games[i]["date"] = datetime.strftime(date, '%b %d')

    # Show user schedule
    return render_template("schedule.html", sport=sport[0]["sport"], captain=captain, games=games, joined=joined)


@app.route("/add_game", methods=["GET", "POST"])
@login_required
@captain_only
def add_game():

    if request.method == "GET":
        return render_template("add_game.html")
    else:
        # Access user input
        date = request.form.get("date")
        time = request.form.get("time")
        opponent = request.form.get("opponent")
        location = request.form.get("location")

        # Add game to schedule
        db.execute("INSERT INTO schedule (team_id, date, time, opponent, location) VALUES(:team_id, :date, :time, :opponent, :location)",
                   team_id=session["team_id"], date=date, time=time, opponent=opponent, location=location)

        return redirect("/schedule")


@app.route("/delete_game", methods=["POST"])
@login_required
@captain_only
def delete_game():
    """ Allow captain to delete a game"""

    # Access game_id
    game_id = request.form.get("delete_game")

    # Delete game from schedule
    db.execute("DELETE FROM schedule WHERE game_id = :game_id",
               game_id=game_id)

    # Delete all information related to game
    db.execute("DELETE FROM playing WHERE game_id = :game_id",
               game_id=game_id)

    # Redirect captain to schedule
    return redirect("/schedule")


@app.route("/change_status", methods=["GET", "POST"])
@login_required
@captain_only
def change_status():
    """Allow captain to change status of game"""

    # Access game_id
    game_id = request.form.get("change_status")

    # Access new status
    new_status = request.form.get("new_status")

    # Change update schedule table
    db.execute("UPDATE schedule SET status = :new_status WHERE game_id = :game_id",
               new_status=new_status, game_id=game_id)

    # Redirect captain to schedule
    return redirect("schedule")


@app.route("/join_team", methods=["GET", "POST"])
@login_required
def join_team():
    """Allow player to join a team"""

    if request.method == "POST":

        # Determine team_id
        sport = request.form.get("sport")
        team = db.execute(
            "SELECT team_id FROM teams WHERE sport = :sport", sport=sport)

        # Insert player into players table
        db.execute("INSERT INTO players (player_id, team_id) VALUES (:player_id, :team_id)",
                   player_id=session["user_id"], team_id=team[0]["team_id"])

        # Redirect player to teams page
        return redirect("/")

    # Show players the teams that he or she can join
    else:
        # Select sports which have a captain
        sports = db.execute(
            "SELECT sport FROM teams WHERE captain_id IS NOT NULL")
        team_id = db.execute(
            "SELECT team_id FROM teams WHERE captain_id IS NOT NULL")

        # Remove the sports that the player has already joined
        pop_counter = 0
        for i in range(len(sports)):
            already_joined = db.execute(
                "SELECT * FROM players WHERE team_id = :team_id AND player_id = :player_id",
                team_id=team_id[i]["team_id"], player_id=session["user_id"])
            if already_joined:
                sports.pop(i - pop_counter)
                # Keep track of how many sports were popped off the list
                pop_counter = pop_counter + 1

        return render_template("join_team.html", sports=sports)


@app.route("/leave_team", methods=["POST"])
@login_required
def leave_team():
    """" Allow player to leave a team """

    # Remove player from table for that team
    db.execute("DELETE FROM players WHERE player_id=:player_id AND team_id = :team_id",
               player_id=session["user_id"], team_id=session["team_id"])

    # Remove player from any games for that team
    db.execute("DELETE FROM playing WHERE player_id = :player_id AND team_id = :team_id", player_id=session["user_id"],
               team_id=session["team_id"])

    # Redirect player to My Teams page
    return redirect("/")


@app.route("/team_players", methods=["GET"])
@login_required
def team_players():
    """ Allow user to view players who have signed up for the team """

    # Select players who have signed up for the team
    players = db.execute(
        "SELECT * FROM users JOIN players ON players.player_id = users.user_id WHERE team_id = :team_id", team_id=session["team_id"])

    # Access sport
    rows = db.execute(
        "SELECT sport FROM teams WHERE team_id = :team_id", team_id=session["team_id"])
    sport = rows[0]["sport"]

    return render_template("players.html", players=players, sport=sport)


@app.route("/remove_player", methods=["POST"])
@login_required
@captain_only
def remove_player():
    """ Allows captain to remove a player from a team """

    # Access player_id
    player_id = request.form.get("remove_player")

    # Delete player from team
    db.execute("DELETE FROM players WHERE player_id = :player_id AND team_id = :team_id",
               player_id=player_id, team_id=session["team_id"])

    # Delete player from any games for team
    db.execute("DELETE FROM playing WHERE player_id = :player_id AND team_id = :team_id",
               player_id=player_id, team_id=session["team_id"])

    # Redirect captain to list of players on team
    return redirect("/team_players")


@app.route("/join_game", methods=["POST"])
@login_required
def join_game():
    """Allow player to join a game"""

    # Access id of game that the player wants to join
    game_id = request.form.get("join_game")

    # Store in table playing that the player signed up to play in the game
    db.execute("INSERT INTO playing (game_id, player_id, team_id) VALUES (:game_id, :player_id, :team_id)",
               game_id=game_id, player_id=session["user_id"], team_id=session["team_id"])
    return redirect("/schedule")


@app.route("/leave_game", methods=["POST"])
@login_required
def leave_game():
    """Allow player to leave a game"""

    # Access id of game that player wants to leave
    game_id = request.form.get("leave_game")

    # Delete corresponing entry in table "playing"
    db.execute("DELETE FROM playing WHERE player_id = :player_id AND game_id = :game_id",
               player_id=session["user_id"], game_id=game_id)

    # Redirect player to his or her schedule
    return redirect("/my_schedule")


@app.route("/my_schedule", methods=["GET"])
@login_required
def my_schedule():
    """Allow player to view schedule of games for which he or she is signed up to play"""

    # Select games for which the player is signed up to play
    games = db.execute(
        "SELECT * FROM schedule JOIN playing ON playing.game_id = schedule.game_id JOIN teams ON teams.team_id = schedule.team_id WHERE playing.player_id = :player_id ORDER BY date", player_id=session["user_id"])

    # Reformat dates to be month day
    for i in range(len(games)):
        date = games[i]["date"]
        date = datetime.strptime(date, "%Y-%m-%d")
        games[i]["date"] = datetime.strftime(date, '%b %d')

    # Show user his schedule
    return render_template("my_schedule.html", games=games)


@app.route("/players", methods=["POST"])
@login_required
def players():
    """ View which players have signed up for a specific game """

    # Access the game id
    game_id = request.form.get("players")

    # Select which players have signed up for that game
    players = db.execute(
        "SELECT * FROM users JOIN playing ON playing.player_id = users.user_id WHERE playing.game_id = :game_id", game_id=game_id)

    # Access the sport and date for that game
    rows = db.execute(
        "SELECT sport, date FROM teams JOIN schedule ON schedule.team_id = teams.team_id WHERE game_id = :game_id", game_id=game_id)
    sport = rows[0]["sport"]
    date = rows[0]["date"]
    date = datetime.strptime(date, "%Y-%m-%d")
    date = datetime.strftime(date, '%b %d')

    # Show user list of players
    return render_template("players.html", players=players, sport=sport, date=date)
