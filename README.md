This is a website for Davenport Intramural Sports teams.

Users register with their name, Yale net id, email address, and a password. They register as either a player or a captain.


Captains can create teams, choosing from a list of sports for which Davenport has teams. When a captain creates a team, it is added to his/her "My Teams."

From the "My Teams" page, the captain can navigate to a page for each of his/her teams. The page displays a schedule, to which the captain can add and delete games for his/her team.
The captain can also view which players have signed up for a specific game.

The captain can also change the status of each game, to one of several options such as "We forfeited" or "Rained out."

Also on the team page is a button "Dissolve Team" which allows the captain to delete the team and all games for it.

Also on the team page is a link to a list of players on the teams, with corresponding email addresses. On that list, the captain has the option for removing a player from the team.


Players can join teams, choosing from a list of teams which have been created by captains.

Once a player joins a team, he/she can access the schedule for that team. The player can join games and view the other players who have signed up for a specific game. The player can also leave the team.

If the player wishes to see the games from any of his/her teams for which he/she signed up, the player can click "My Schedule." This directs the user to page that displays all of the games for which he or she signed up.
There is also an option for the player to leave a game.




