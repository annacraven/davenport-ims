# helpers.py
# created by Anna Craven on 11/27/17
# based on code written by CS50 staff from pset7

import csv
import urllib.request
from cs50 import SQL

from flask import redirect, render_template, request, session
from functools import wraps


def apology(message, code=400):
    """Renders message as an apology to user."""
    def escape(s):
        """
        Escape special characters.

        https://github.com/jacebrowning/memegen#special-characters
        """
        for old, new in [("-", "--"), (" ", "-"), ("_", "__"), ("?", "~q"),
                         ("%", "~p"), ("#", "~h"), ("/", "~s"), ("\"", "''")]:
            s = s.replace(old, new)
        return s
    return render_template("apology.html", bottom=code, top=escape(message)), code


def login_required(f):
    """
    Decorate routes to require login.

    http://flask.pocoo.org/docs/0.12/patterns/viewdecorators/
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get("user_id") is None:
            return redirect("/login")
        return f(*args, **kwargs)
    return decorated_function

def captain_only(f):
    """
    Decorate routes to be accessible only to captains
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get("bool_captain") == 0:
            return apology("You must be a captain to visit this page")
        else:
            return f(*args, **kwargs)

    return decorated_function
