I implemented my project using a database called dport.db. It has 5 tables: users, teams, players, schedule, and playing.
I created my database in this way because I wanted to store the site's users, teams, users who have signed up for a certain team (players), games created by captains (schedule),
and games that users joined (playing).

When a user registers, his/her information is stored in users. users also notes whether or not the player is a captain (1 for captain, 0 for player; in the column bool_captain).

When a user logs in, his/her user_id and bool_captain is stored in session.

I used two decorators, @login_required for routes that require a user to be logged in, and @captain_only for routes that are only accessible to captains.

When a captain wants to creates a team, his/her id is assigned to that team in teams. Once a team in teams has a captain_id assigned to it, it becomes available for a player to join.

When a player joins a team, his/her user_id is stored in players with the team_id of the team he/she joined.

The My Teams page checks whether the user is a player or a captain. If player, palyers is queried to find the teams to which the player belongs. If captain, teams is queried to find the teams which the captain manages.
The teams associated with the user are displayed.

When a user clicks on a team on the page My Teams, the team_id for that team is stored in session. Then the user is directed to a schedule for that team which also contains other information.
/team_players accesses players joined with users to display a list of players for that team, the team_id stored in session.
If session indicates that the user is a captain, he/she can also view the email addresses of the players, and he/she has the option to remove a player from the team.
If the captain removes a player from the team, that player is deleted from players and playing where team_id = team_id stored in session.

When a captain adds a game, a new row is added to schedule. /my_schedule and /schedule both access schedule to create a table of games.

Captains can change the status of a game or delete it. Each "change status" and "delete" button has a value associated with it that is the game_id.
When a captain changes the status of game or deletes a game, schedule is updated where game_id = the game_id of the button that the captain clicked.

When a player clicks a join game button, a new row is added to playing that stores the player, the game, and the team.
In order to display the join game buttons only for games that the player has not yet joined, I created an empty listed joined. For each game for that team, playing is queried for an entry for that player and that game.
If there is an entry, that means the player is already playing in that game, so joined gets a 1 added to it that corresponds to that game. If there is not an entry, joined gets a 0 added to it.
Then, for each game, if joined is 1, the Join Game button is replace with text "Joined." This ensures that a player cannot join a game more than once.

When a player joins a game, a new row is added to playing. For each game in a schedule, there is a "leave game" button. Each leave game button has a game_id associated with it.
When a player clicks a leave game button, the row in playing with that game_id and the player's user_id is deleted.

For each game in a schedule, there is a button "Players" with a game_id associated with it. When a user clicks that button, players joined with users is queried for entries with that game_id.
The resulting names are displayed.

When a captain clicks dissolve team, he/she first must confirm his/her intentions. I did this because I did not want anyone to accidentally dissolve their team.
When a captain confirms that he/she wants to dissolve the team, all entries in players, playing, and schedule associated with that team_id are deleted.

The main colors of the site are maroon, black, and white. I chose these because they are Davenport's colors.